package nikola.com.gamesapplication.data;

import android.arch.lifecycle.MutableLiveData;
import java.util.ArrayList;
import nikola.com.gamesapplication.pojo.Example;
import nikola.com.gamesapplication.pojo.ListGames;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitRpository {

    private static final String BASE_URL = "http://mobilews.365scores.com/";

    private MutableLiveData<ArrayList<ListGames>> mMutableListGames;
    private ArrayList<ListGames> mRrealListGames;

    public RetrofitRpository() {

    }

    public MutableLiveData<ArrayList<ListGames>> getListOfData() {
        if (mMutableListGames == null) {
            mMutableListGames = new MutableLiveData<>();
            mRrealListGames = new ArrayList<ListGames>();

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create());
            Retrofit retrofit = builder.build();
            ListGamesCLient client = retrofit.create(ListGamesCLient.class);
            Call<Example> call = client.reposForUser();
            call.enqueue(new Callback<Example>() {
                @Override
                public void onResponse(Call<Example> call, Response<Example> response) {
                    Example example = response.body();
                    for (int i = 0; i < example.getCompetitions().size(); i++) {
                        mRrealListGames.add(example.getCompetitions().get(i));
                        for (int j = 0; j < example.getGames().size(); j++) {
                            if (example.getGames().get(j).getComp().equals(example.getCompetitions()
                                    .get(i).getiD())) {
                                mRrealListGames.add(example.getGames().get(j));
                            }
                        }
                    }
                    mMutableListGames.setValue(mRrealListGames);
                }

                @Override
                public void onFailure(Call<Example> call, Throwable t) {

                }
            });

        }
        return mMutableListGames;
    }
}