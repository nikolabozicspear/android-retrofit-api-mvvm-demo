package nikola.com.gamesapplication.data;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import java.util.ArrayList;
import nikola.com.gamesapplication.pojo.ListGames;

public class ListGamesViewModel extends ViewModel {
    private RetrofitRpository mRepository;

    public ListGamesViewModel(RetrofitRpository mRepository) {
        this.mRepository = mRepository;
    }

    public LiveData<ArrayList<ListGames>> getListItems() {
        return (LiveData<ArrayList<ListGames>>) mRepository.getListOfData();
    }
}
