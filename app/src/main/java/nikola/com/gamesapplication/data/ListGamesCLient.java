package nikola.com.gamesapplication.data;
import nikola.com.gamesapplication.pojo.Example;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Bolt on 2/15/2018.
 */

public interface ListGamesCLient {
    @GET("Data/Games/?lang=1&uc=6&tz=15&countries=1")
    Call<Example> reposForUser();
}