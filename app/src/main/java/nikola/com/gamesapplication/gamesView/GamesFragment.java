package nikola.com.gamesapplication.gamesView;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import javax.inject.Inject;

import nikola.com.gamesapplication.GamesApplication;
import nikola.com.gamesapplication.R;
import nikola.com.gamesapplication.data.ListGamesViewModel;
import nikola.com.gamesapplication.pojo.ListGames;

/**
 * A simple {@link Fragment} subclass.
 */
public class GamesFragment extends Fragment {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    ListGamesViewModel mListItemViewModel;

    private RecyclerView mRecyclerView;
    private ArrayList<ListGames> mListOfData;
    private GamesAdapter mAdapter;
    private View v;

    public static GamesFragment newInstance() {
        GamesFragment fragment = new GamesFragment();
        return fragment;
    }

    public GamesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((GamesApplication) this.getActivity().getApplication())
                .getApplicationComponent()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_games, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.rec_list_activity);
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListItemViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ListGamesViewModel.class);

        mListItemViewModel.getListItems().observe(this, new Observer<ArrayList<ListGames>>() {
            @Override
            public void onChanged(@Nullable ArrayList<ListGames> listGames) {
                setListData(listGames);
            }
        });
    }

    public void setListData(ArrayList<ListGames> listOfData) {
        this.mListOfData = listOfData;

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new GamesAdapter(getContext(), listOfData);
        mRecyclerView.setAdapter(mAdapter);
    }
}
