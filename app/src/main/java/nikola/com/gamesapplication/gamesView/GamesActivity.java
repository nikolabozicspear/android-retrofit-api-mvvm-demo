package nikola.com.gamesapplication.gamesView;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import nikola.com.gamesapplication.BaseActivity;
import nikola.com.gamesapplication.R;

public class GamesActivity extends BaseActivity {

    private static final String DETAIL_FRAG = "GAMES_FRAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        FragmentManager manager = getSupportFragmentManager();
        GamesFragment fragment = (GamesFragment) manager.findFragmentByTag(DETAIL_FRAG);

        if (fragment == null) {
            fragment = GamesFragment.newInstance();
        }

        addFragmentToActivity(manager,
               fragment,
               R.id.root_activity_detail,
                DETAIL_FRAG
       );
    }
}
