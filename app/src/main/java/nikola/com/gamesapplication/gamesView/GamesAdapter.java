package nikola.com.gamesapplication.gamesView;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import nikola.com.gamesapplication.R;
import nikola.com.gamesapplication.pojo.Competition;
import nikola.com.gamesapplication.pojo.Game;
import nikola.com.gamesapplication.pojo.ListGames;

/**
 * Created by Bolt on 2/15/2018.
 */

public class GamesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<ListGames> mListOfData;
    private View v;

    public GamesAdapter(Context context, ArrayList<ListGames> listOfData) {
        this.mListOfData = listOfData;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        switch (viewType) {
            case 0: {
                v = inflater.inflate(R.layout.game_view, parent, false);
                return new GameViewHoder(v);
            }
            case 1: {
                v = inflater.inflate(R.layout.competition_view, parent, false);
                return new CompetitionViewHoder(v);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListGames currentItem = mListOfData.get(position);

        if (currentItem instanceof Game) {
            boolean hasScore = ((Game) currentItem).getScrs().get(0) >= 0;
            boolean homeWins = (((Game) currentItem).getScrs().get(0) > (((Game) currentItem)
                    .getScrs().get(1)));
            boolean even = (((Game) currentItem).getScrs().get(0).equals(((Game) currentItem)
                    .getScrs().get(1)));


            String homeTeam = getTeamParsed(((Game) currentItem).getComps().get(0).getName());
            String awayTeam = getTeamParsed(((Game) currentItem).getComps().get(01).getName());
            ((GameViewHoder) holder).teamHome.setText(homeTeam);
            ((GameViewHoder) holder).teamAway.setText(awayTeam);

            ((GameViewHoder) holder).scoreHome.setText(hasScore ? String.valueOf
                    (((Game) currentItem).getScrs().get(0)) : "");
            ((GameViewHoder) holder).scoreAway.setText(hasScore ? String.valueOf
                    (((Game) currentItem).getScrs().get(1)) : "");
            ((GameViewHoder) holder).coma.setText(hasScore ? ":" : getTime(((Game) currentItem)
                    .getsTime()));
            ((GameViewHoder) holder).ended.setText(hasScore ? "ended" : "");

            String urlHome = mContext.getResources().getString(R.string.team_logo_url).toString()
                    + "" + ((Game) currentItem).getComps().get(0).getID();
            String urlaway = mContext.getResources().getString(R.string.team_logo_url).toString()
                    + "" + ((Game) currentItem).getComps().get(1).getID();
            Picasso.with(mContext).load(urlHome).into(((GameViewHoder) holder).logoHome);
            Picasso.with(mContext).load(urlaway).into(((GameViewHoder) holder).logoAway);

            ((GameViewHoder) holder).scoreHome.setTextColor(hasScore && homeWins ?
                    Color.GREEN : Color.WHITE);
            ((GameViewHoder) holder).scoreAway.setTextColor(hasScore && !homeWins ?
                    Color.GREEN : Color.WHITE);
            ((GameViewHoder) holder).teamHome.setTextColor(hasScore && homeWins ?
                    Color.GREEN : Color.WHITE);
            ((GameViewHoder) holder).teamAway.setTextColor(hasScore && !homeWins ?
                    Color.GREEN : Color.WHITE);

            if (hasScore && even) {
                ((GameViewHoder) holder).scoreHome.setTextColor(Color.GREEN);
                ((GameViewHoder) holder).scoreAway.setTextColor(Color.GREEN);
                ((GameViewHoder) holder).teamHome.setTextColor(Color.GREEN);
                ((GameViewHoder) holder).teamAway.setTextColor(Color.GREEN);
            }

        } else {
            ((CompetitionViewHoder) holder).competitionLabel.setText(((Competition) currentItem)
                    .getName());
            String url = mContext.getResources().getString(R.string.country_logo_url) + "" +
                    ((Competition) currentItem).getcID();
            Picasso.with(mContext).load(url).into(((CompetitionViewHoder) holder).logoCompetition);
        }
    }

    @Override
    public int getItemCount() {
        return mListOfData.size();
    }

    class GameViewHoder extends RecyclerView.ViewHolder {

        private TextView teamHome;
        private TextView teamAway;
        private TextView scoreHome;
        private TextView scoreAway;
        private ImageView logoHome;
        private ImageView logoAway;
        private TextView coma;
        private TextView ended;

        public GameViewHoder(View itemView) {
            super(itemView);
            this.teamHome = (TextView) itemView.findViewById(R.id.team_home);
            this.teamAway = (TextView) itemView.findViewById(R.id.team_away);
            this.scoreHome = (TextView) itemView.findViewById(R.id.score_home);
            this.coma = (TextView) itemView.findViewById(R.id.coma);
            this.ended = (TextView) itemView.findViewById(R.id.ended);
            this.scoreAway = (TextView) itemView.findViewById(R.id.score_away);
            this.logoHome = (ImageView) itemView.findViewById(R.id.logo_home);
            this.logoAway = (ImageView) itemView.findViewById(R.id.logo_away);
        }
    }

    public static class CompetitionViewHoder extends RecyclerView.ViewHolder {

        private TextView competitionLabel;
        private ImageView logoCompetition;

        public CompetitionViewHoder(View itemView) {
            super(itemView);
            competitionLabel = (TextView) itemView.findViewById(R.id.cometition_label);
            logoCompetition = (ImageView) itemView.findViewById(R.id.image_competition);
        }
    }

    public int getItemViewType(int position) {
        ListGames currentItem = mListOfData.get(position);
        if (mListOfData != null && currentItem != null) {
            return currentItem instanceof Game ? ((Game) currentItem).getListType() :
                    ((Competition) currentItem).getListType();
        }
        return 0;
    }

    public String getTime(String time) {
        Date d1 = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat formatOutput = new SimpleDateFormat("dd/MM hh:mm");
        try {
            d1 = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatOutput.format(d1.getTime());
    }

    public String getTeamParsed(String team) {
        String teamPArsed = "";
        if (team.length() > 16) {
            teamPArsed = team.indexOf(" ") > 0 ? team.substring(0, team.indexOf(" ")) + "\n" +
                    team.substring(team.indexOf(" ") + 1, + team.length()) : team.substring(0, 16)
                    + "-\n" +  team.substring(team.indexOf(" ") + 1, team.length());
            return teamPArsed;
        } else {
            return team;
        }
    }
}
