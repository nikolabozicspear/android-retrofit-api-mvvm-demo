package nikola.com.gamesapplication.dagger2;

import android.app.Application;
import javax.inject.Singleton;
import dagger.Component;
import nikola.com.gamesapplication.gamesView.GamesFragment;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(GamesFragment gamesFragment);
    Application application();
}
