package nikola.com.gamesapplication.dagger2;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nikola.com.gamesapplication.GamesApplication;
import nikola.com.gamesapplication.data.CustomViewModelFactory;
import nikola.com.gamesapplication.data.RetrofitRpository;

@Module
public class ApplicationModule {
    private final GamesApplication mGamesApplication;

    public ApplicationModule(GamesApplication application) {
        mGamesApplication = application;
    }

    @Provides
    Application provideApplication(){
        return mGamesApplication;
    }

    @Provides
    @Singleton
    RetrofitRpository provideListItemRepository(){
        return new RetrofitRpository();
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory provideViewModelFactory(RetrofitRpository repository){
        return new CustomViewModelFactory(repository);
    }
}
