package nikola.com.gamesapplication.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Bolt on 2/14/2018.
 */

public class Competition extends ListGames {

    public static final int LIST_TYPE = 1;

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("CID")
    @Expose
    private Integer cID;

    public static int getListType() {
        return LIST_TYPE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public Integer getcID() {
        return cID;
    }

    public void setcID(Integer cID) {
        this.cID = cID;
    }
}
