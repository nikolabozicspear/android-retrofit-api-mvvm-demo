package nikola.com.gamesapplication.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bolt on 2/13/2018.
 */

public class Comp {
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("CID")
    @Expose
    private Integer cID;
    @SerializedName("SID")
    @Expose
    private Integer sID;
    @SerializedName("Trend")
    @Expose
    private List<Integer> trend = null;
    @SerializedName("HasSquad")
    @Expose
    private Boolean hasSquad;
    @SerializedName("Type")
    @Expose
    private Integer type;
    @SerializedName("SName")
    @Expose
    private String sName;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCID() {
        return cID;
    }

    public void setCID(Integer cID) {
        this.cID = cID;
    }

    public Integer getSID() {
        return sID;
    }

    public void setSID(Integer sID) {
        this.sID = sID;
    }

    public List<Integer> getTrend() {
        return trend;
    }

    public void setTrend(List<Integer> trend) {
        this.trend = trend;
    }

    public Boolean getHasSquad() {
        return hasSquad;
    }

    public void setHasSquad(Boolean hasSquad) {
        this.hasSquad = hasSquad;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSName() {
        return sName;
    }

    public void setSName(String sName) {
        this.sName = sName;
    }
}
