package nikola.com.gamesapplication.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Bolt on 2/14/2018.
 */


public class Example {
    @SerializedName("LastUpdateID")
    @Expose
    private Integer lastUpdateID;
    @SerializedName("Games")
    @Expose
    private List<Game> games = null;
    @SerializedName("Competitions")
    @Expose
    private List<Competition> competitions = null;

    public Integer getLastUpdateID() {
        return lastUpdateID;
    }

    public void setLastUpdateID(Integer lastUpdateID) {
        this.lastUpdateID = lastUpdateID;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public List<Competition> getCompetitions() {
        return competitions;
    }

    public void setCompetitions(List<Competition> competitions) {
        this.competitions = competitions;
    }

}


