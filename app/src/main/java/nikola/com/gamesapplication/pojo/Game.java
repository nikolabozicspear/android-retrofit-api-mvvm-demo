package nikola.com.gamesapplication.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bolt on 2/13/2018.
 */

public class Game extends ListGames {

    public static int getListType() {
        return LIST_TYPE;
    }

    public static final int LIST_TYPE = 0;

    @SerializedName("ID")
    @Expose
    private Integer iD;

    @SerializedName("Comp")
    @Expose
    private Integer comp;

    @SerializedName("Active")
    @Expose
    private Boolean active;

    @SerializedName("GT")
    @Expose
    private Integer gT;

    @SerializedName("STime")
    @Expose
    private String sTime;

    @SerializedName("Comps")
    @Expose
    private List<Comp> comps = null;

    @SerializedName("Scrs")
    @Expose
    private List<Integer> scrs = null;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getComp() {
        return comp;
    }

    public void setComp(Integer comp) {
        this.comp = comp;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getGT() {
        return gT;
    }

    public void setGT(Integer gT) {
        this.gT = gT;
    }

    public List<Integer> getScrs() {
        return scrs;
    }

    public void setScrs(List<Integer> scrs) {
        this.scrs = scrs;
    }

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public Integer getgT() {
        return gT;
    }

    public void setgT(Integer gT) {
        this.gT = gT;
    }

    public String getsTime() {
        return sTime;
    }

    public void setsTime(String sTime) {
        this.sTime = sTime;
    }

    public List<Comp> getComps() {
        return comps;
    }

    public void setComps(List<Comp> comps) {
        this.comps = comps;
    }
}
