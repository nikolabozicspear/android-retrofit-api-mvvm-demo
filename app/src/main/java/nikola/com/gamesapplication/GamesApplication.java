package nikola.com.gamesapplication;

import android.app.Application;
import nikola.com.gamesapplication.dagger2.ApplicationComponent;
import nikola.com.gamesapplication.dagger2.ApplicationModule;
import nikola.com.gamesapplication.dagger2.DaggerApplicationComponent;


public class GamesApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
